def attr_to_str(attr):
    result = ''
    for key, value in attr.items():
        result += ' ' + key + '="' + value + '"'
    return result


class HtmlObject:
    def __init__(self, type, parent=None, childless=False, attr=None):
        if attr is None:
            attr = {}
        self.type = type
        self.parent = parent
        if self.parent is not None:
            self.parent.add_child(self)
        self.childless = childless
        self.attr = attr
        self.childs = []

    def add_child(self, child):
        self.childs.append(child)

    def get_child(self, index):
        return self.childs[index]

    def add_attr(self, key, value):
        self.attr[key] = value

    def __str__(self):
        if self.childless:
            return '\n<' + self.type + attr_to_str(self.attr) + '>\n'
        else:
            childs_str = ''
            for child in self.childs:
                childs_str += str(child)
            if childs_str != '':
                childs_str = '\t' + '\t'.join(childs_str.splitlines(True))
            return '\n<' + self.type + attr_to_str(self.attr) + '>\n' + childs_str + '\n</' + self.type + '>\n'


if __name__ == '__main__':

    with open('input.csv', 'r') as input_file:
        rows = input_file.read().splitlines()
        html = HtmlObject('html')
        body = HtmlObject('body', parent=html)
        table = HtmlObject('table', parent=body)
        for row in rows:
            html_row = HtmlObject('tr', parent=table)
            for col in row.split(','):
                item = HtmlObject('td', parent=html_row)
                try:
                    if col[0] == '*' and col[-1] == '*':
                        item.add_attr('style', 'font-weight:bold')
                        item.add_child(col[1:-1])
                    elif col[0] == '_' and col[-1] == '_':
                        item.add_attr('style', 'text-decoration: underline;')
                        item.add_child(col[1:-1])
                    else:
                        item.add_child(col)
                except IndexError:
                    pass  # this item is empty
        with open('task5.html', "w") as output_file:
            output_file.write(str(html))
