from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.home_page, name='home-page'),
    url(r'^task/(?P<pk>\d+)/details$', views.task_detail, name='task-detail'),
]
