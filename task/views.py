from django.shortcuts import render


def home_page(request):
    return render(request, 'home_page.html', {})


def task_detail(request, pk):
    return render(request, 'task_detail.html', {'pk': pk})
